import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersService } from "./users.service";
import { AddFormComponent } from './users/add-form/add-form.component';
import { UpdateFormComponent } from './users/update-form/update-form.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    AddFormComponent,
    UpdateFormComponent
  ],
  imports: [
    BrowserModule,
     HttpModule,
      FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
    {path: '', component: ProductsComponent},
            {path: 'products', component: ProductsComponent},
            {path: 'users', component: UsersComponent},
            {path: 'update-form/:id', component: UpdateFormComponent},
            {path: '**', component: NotFoundComponent}
          ])
  ],
  
  providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
